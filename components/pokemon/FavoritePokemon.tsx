import { Card, Grid } from '@nextui-org/react';
import React, { FC, PropsWithChildren } from 'react';
import FavoriteCardPokemon from './FavoriteCardPokemon';

interface Props {
  listPokemons: number[];
}

const FavoritePokemon: FC<PropsWithChildren<Props>> = ({ listPokemons }) => {
  return (
    <Grid css={{ display: 'flex' }}>
      {listPokemons.map((id: number) => (
        <FavoriteCardPokemon id={id} key={id} />
      ))}
    </Grid>
  );
};

export default FavoritePokemon;
