import { Card, Grid, Row, Text } from '@nextui-org/react';
import { useRouter } from 'next/router';
import React, { FC, PropsWithChildren } from 'react';
import { SmallPokemon } from '../../interfaces';

interface Props {
  pokemon: SmallPokemon;
}

const PokemonCard: FC<PropsWithChildren<Props>> = ({ pokemon }) => {
  const router = useRouter();

  const { id, name, img } = pokemon;

  const handleClick = () => {
    router.push(`/pokemon/${name}`);
  };

  return (
    <Grid xs={6} sm={3} md={3} xl={1} key={id} onClick={handleClick}>
      <Card isHoverable isPressable variant='bordered'>
        <Card.Body css={{ p: 1 }}>
          <Card.Image src={img} width='100%' height={140} />
        </Card.Body>
        <Card.Footer>
          <Row justify='space-between'>
            <Text transform='capitalize'>{name}</Text>
            <Text>#{id}</Text>
          </Row>
        </Card.Footer>
      </Card>
    </Grid>
  );
};

export default PokemonCard;
