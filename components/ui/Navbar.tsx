import { Image, Spacer, Text, useTheme, Link } from '@nextui-org/react';
import NextLink from 'next/link';

const Navbar = () => {
  return (
    <div
      style={{
        alignItems: 'center',
        backgroundColor: '#0d0f18',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'start',
        padding: '0px 20px',
        width: '100%',
      }}
    >
      <Image
        src='https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/25.png'
        alt='pikachu'
        css={{ width: '70px' }}
      />
      <NextLink href={'/'} passHref>
        <Link>
          <Text color='white' h2>
            P
          </Text>
          <Text color='white' h3>
            okemon
          </Text>
        </Link>
      </NextLink>
      <Spacer css={{ flex: 1 }} />
      <NextLink href={'/favorites'} passHref>
        <Link>
          <Text>Favorites</Text>
        </Link>
      </NextLink>
    </div>
  );
};

export { Navbar };
