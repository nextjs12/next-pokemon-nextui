/* eslint-disable jsx-a11y/alt-text */
import { Container, Image, Text } from '@nextui-org/react';
import React from 'react';

const NoFavorites = () => {
  return (
    <Container
      css={{
        display: 'flex',
        flexDirection: 'column',
        height: 'calc(100vh - 100px )',
      }}
    >
      <Text>No Favorites</Text>
      <Image
        src='https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/132.svg'
        width={250}
        height={250}
        css={{ opacity: 0.1 }}
      />
    </Container>
  );
};

export default NoFavorites;
