import Head from 'next/head';
import React, { FC, PropsWithChildren } from 'react';
import { Navbar } from '../ui';
// import { Navbar } from '../ui';

interface PropsChildren {
  title?: string;
}

const origin = typeof window === 'undefined' ? '' : window.location.origin;

const Layout: FC<PropsWithChildren<PropsChildren>> = ({ children, title }) => {
  return (
    <>
      <Head>
        <title>{title}</title>
        <meta name='author' content='wisftock' />
        <meta name='description' content='Informacion sobre el pokemon' />
        <meta name='keywords' content={`${title} palabras claves`} />

        <meta
          property='og:title'
          content='How to Become an SEO Expert (8 Steps)'
        />
        <meta
          property='og:description'
          content='Get from SEO newbie to SEO pro in 8 simple steps.'
        />
        <meta property='og:image' content={`${origin}/img/no-image.jpg`} />
      </Head>
      <Navbar />
      <main style={{ padding: '0px 50px' }}>{children}</main>
    </>
  );
};

export default Layout;
