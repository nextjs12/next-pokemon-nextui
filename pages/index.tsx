import type { GetStaticProps, NextPage } from 'next';
import { Grid } from '@nextui-org/react';
import { pokeApi } from '../api';
import Layout from '../components/layouts/Layout';
import { PokeResult, SmallPokemon } from '../interfaces';
import PokemonCard from '../components/pokemon/PokemonCard';

interface Props {
  pokemon: SmallPokemon[];
}

const Home: NextPage<Props> = ({ pokemon }) => {
  return (
    <Layout title='list the pokemon'>
      <Grid.Container gap={2} justify='flex-start'>
        {pokemon.map((item) => (
          <PokemonCard pokemon={item} key={item.name} />
        ))}
      </Grid.Container>
    </Layout>
  );
};

// usarlo para tener precargado los datos
export const getStaticProps: GetStaticProps = async (ctx) => {
  const { data } = await pokeApi.get<PokeResult>('/pokemon?limit=151');
  const pokemon: SmallPokemon[] = data.results.map((item, index) => {
    return {
      ...item,
      id: `${index + 1}`,
      img: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/${
        index + 1
      }.svg`,
    };
  });
  return {
    props: {
      pokemon,
    },
  };
};

export default Home;
