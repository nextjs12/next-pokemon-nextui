import { Card, Container, Grid, Image, Text } from '@nextui-org/react';
import { useEffect, useState } from 'react';
import Layout from '../../components/layouts/Layout';
import FavoritePokemon from '../../components/pokemon/FavoritePokemon';
import NoFavorites from '../../components/ui/NoFavorites';
import { pokemons } from '../../utils/LocalFavorites';

const FavoritesPage = () => {
  const [favoritesPokemons, setFavoritesPokemons] = useState<number[]>([]);

  useEffect(() => {
    setFavoritesPokemons(pokemons());
  }, []);

  return (
    <Layout title='Pokémons - Favoritos'>
      {favoritesPokemons.length === 0 ? (
        <NoFavorites />
      ) : (
        <FavoritePokemon listPokemons={favoritesPokemons} />
      )}
    </Layout>
  );
};

export default FavoritesPage;
