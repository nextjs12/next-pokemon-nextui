import React, { useState } from 'react';
import { Button, Card, Container, Grid, Image, Text } from '@nextui-org/react';
import { GetStaticProps, NextPage, GetStaticPaths } from 'next';

import Layout from '../../components/layouts/Layout';
import { PokemonFull } from '../../interfaces';
import { exitFavorites, toggleFavorites } from '../../utils';
import confetti from 'canvas-confetti';
import { getPokemonInputInfo } from '../../utils/getPkemonInput';

interface Props {
  pokemon: PokemonFull;
}

const PokemonPage: NextPage<Props> = ({ pokemon }) => {
  const [isInfavorites, setIsInfavorites] = useState(exitFavorites(pokemon.id));

  // console.log(isInfavorites);

  const handleClick = () => {
    toggleFavorites(pokemon.id);
    setIsInfavorites(!isInfavorites);
    confetti({
      particleCount: 100,
      spread: 150,
      angle: -90,
      origin: { x: 0.5, y: 0 },
    });
  };

  return (
    <Layout title='Pokemon'>
      <Grid.Container css={{ marginTop: '5px' }} gap={2}>
        <Grid xs={12} sm={4}>
          <Card isHoverable css={{ padding: '30px' }}>
            <Card.Body>
              <Card.Image
                src={
                  pokemon.sprites.other?.dream_world.front_default ||
                  '/no-image.png'
                }
                alt={pokemon.name}
                width='100%'
                height={200}
              />
            </Card.Body>
          </Card>
        </Grid>
        <Grid xs={12} sm={8}>
          <Card>
            <Card.Header
              css={{ display: 'flex', justifyContent: 'space-between' }}
            >
              <Text h1 transform='capitalize'>
                {pokemon.name}
              </Text>
              <Button
                color='gradient'
                ghost={!isInfavorites}
                onClick={handleClick}
              >
                {isInfavorites ? 'to favorites' : 'Save to favorite'}
              </Button>
            </Card.Header>
            <Card.Body>
              <Text size={30}>Sprites:</Text>
              <Container direction='row' display='flex'>
                <Image
                  src={pokemon.sprites.front_default}
                  alt={pokemon.name}
                  width={100}
                  height={200}
                />
                <Image
                  src={pokemon.sprites.back_default}
                  alt={pokemon.name}
                  width={100}
                  height={200}
                />
                <Image
                  src={pokemon.sprites.front_shiny}
                  alt={pokemon.name}
                  width={100}
                  height={200}
                />
                <Image
                  src={pokemon.sprites.back_shiny}
                  alt={pokemon.name}
                  width={100}
                  height={200}
                />
              </Container>
            </Card.Body>
          </Card>
        </Grid>
      </Grid.Container>
    </Layout>
  );
};

export const getStaticPaths: GetStaticPaths = async (ctx) => {
  const pokemon = [...Array(151)].map((value, index) => `${index + 1}`);

  return {
    paths: pokemon.map((id) => {
      return {
        params: {
          id,
        },
      };
    }),
    fallback: false,
  };
};

export const getStaticProps: GetStaticProps = async (ctx) => {
  const { id } = ctx.params as { id: string };

  const pokemon = await getPokemonInputInfo(id);
  return {
    props: {
      pokemon,
    },
  };
};

export default PokemonPage;
